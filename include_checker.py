#!/usr/bin/env python3
from __future__ import annotations

import os
import subprocess
from timeit import default_timer as timer
from datetime import timedelta
import argparse
from typing import List, Dict, Any


class ResultType:
    def __init__(self, passed=False, time=0.0, header=None):
        self.passed = passed
        self.time = time
        self.header = header
        self.main_file: str = 'main.cpp'

    def __str__(self):
        return '[{}] [{:.2f}s] {}'.format('SUCCESS' if self.passed else 'FAILED ', self.time, self.header)

    def __repr__(self):
        return 'ResultType({}, {}, {})'.format(self.passed, self.time, self.header)


class Analyzer:
    """
    Check if the given headers include everything they refer to (at least indirectly).
    :param headers: Header files to analyze.
    :param build_folder: Build folder for the test program. Needs to be ready to build using :param build_command.
    :param main_file: Source file to build. Default: <build_folder/../main.cpp>.
    :param build_command: The command to build the test program inside :param build_folder. Default: <make>
    :param options: Additional options.
    """
    def __init__(self, *,
                 headers: List[str],
                 options: Dict[str, Any],
                 main_file: str,
                 build_folder: str,
                 build_command: str,
                 ):
        self.options = options
        self.headers = headers
        self.main_file = main_file
        self.build_folder = build_folder
        self.build_command = build_command

        if not os.path.isabs(self.main_file):
            self.main_file = self.build_folder + '/' + self.main_file

        self.results: List[ResultType] = []

    def print_status(self, *args, **kwargs):
        """
        Print args if user requested it.
        """
        if 'status' in self.options['print']:
            print(*args, **kwargs)

    def print_log(self, *args):
        """
        Print args if user requested it.
        """
        if 'log' in self.options['print']:
            print(*args)

    def filter_headers(self):
        """
        Apply filter based on options.
        """
        start = self.options['start']
        end = len(self.headers)
        self.headers = self.headers[start:end]

    def run(self) -> bool:
        self.filter_headers()

        self.print_status('Initialize')
        self.generate_main()
        if not self.build_main():
            self.print_log('Could not build main without any headers.')
            return False

        # TODO parallel
        for i, header in enumerate(self.headers):
            self.print_status('[{}/{}] Check header file:'.format(i + 1, len(self.headers)), header, end='\x1b[1K\r')

            self.generate_main(header)

            current = ResultType()
            current.header = header

            start = timer()
            current.passed = self.build_main()
            end = timer()

            current.time = timedelta(seconds=end - start).total_seconds()

            if not current.passed:
                self.print_status('Could not compile with header:', header)

            self.print_status('[{}/{}] {}'.format(i + 1, len(self.headers), current))

            self.results.append(current)

        # Write failed includes into main
        self.generate_main([r.header for r in self.results if not r.passed])

        return self.success()

    def generate_main(self, header: List[str] | str = None):
        """
        Add the header to the includes of the main file.
        :param header: Header to include
        """
        if header is None:
            header = []
        elif isinstance(header, str):
            header = [header]

        cpp = [r'#include <' + h + '>' for h in header] + ['int main() {}']

        cpp = '\n'.join(cpp)
        self.print_log(f'Write \n{cpp}\n to {self.main_file}')
        with open(self.main_file, 'w') as f:
            f.write(cpp)
            f.flush()
            f.close()

    def build_main(self) -> bool:
        """
        Build executable.
        :return: True on successful build, False otherwise
        """
        self.print_log('Run:', self.build_command)
        ret = subprocess.Popen(self.build_command.split(), cwd=self.build_folder,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               universal_newlines=True)
        stdout, stderr = ret.communicate()

        if ret.returncode != 0:
            self.print_log(stdout)
            return False
        else:
            return True

    def print_results(self):
        """
        Print results for each header.
        """
        passed = [(i, r) for i, r in enumerate(self.results) if r.passed]
        failed = [(i, r) for i, r in enumerate(self.results) if not r.passed]

        passed.sort(key=lambda ir: ir[1].time, reverse=False)
        if 'passed' in self.options['print']:
            for i, r in passed:
                print(i, r)

        if 'failed' in self.options['print']:
            for i, r in failed:
                print(i, r)

    def success(self) -> bool:
        """
        :return: True if all headers are buildable.
        """
        return all((r.passed for r in self.results))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Check self-consistency of C++ header files.')
    parser.add_argument('headers', type=str, help='Header file(s) to analyze', nargs='*', metavar='headers')
    parser.add_argument('--build', type=str, help='Build directory. Needs to be ready for build.', required=True)
    parser.add_argument('--main', type=str, help='Source file for compilation. Default: $build/../main.cpp')
    parser.add_argument('--tool', type=str, help='Build tool. Default: <cmake --build . --target >. '
                                                 'Target gets appended to end of command.',
                        default='cmake --build . --target ',)
    parser.add_argument('--target', type=str, help='Build target. Default: <test_includes>', default='test_includes',
                        required=True)

    print_choices = ('failed', 'passed', 'status', 'log')
    parser.add_argument('--print', choices=print_choices, default=None, nargs='*',
                        help='Select what to print.')
    parser.add_argument('--start', type=int, help='Start at given index.', default=0)

    args = parser.parse_args()

    headers = args.headers
    build_folder = os.path.abspath(args.build)
    build_command = args.tool + ' ' + args.target
    main_file = args.main
    target = args.target

    print_args = []
    if args.print is None:
        print_args = []
    else:
        if len(args.print) == 0:
            print_args = print_choices
        else:
            print_args = args.print

    options = {
        'print': print_args,
        'start': args.start,
    }

    if main_file is None:
        main_file = build_folder + '/../main.cpp'

    analyzer = Analyzer(
        headers=headers,
        options=options,
        main_file=main_file,
        build_folder=build_folder,
        build_command=build_command,
    )

    analyzer.run()
    analyzer.print_results()

    exit(0 if analyzer.success() else -1)
